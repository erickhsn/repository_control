﻿using repository_control.Helpers;
using repository_control.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace repository_control.Controllers
{
    public class HomeController : Controller
    {
        private AplicationDbContext db = new AplicationDbContext();
        public ActionResult Index(string sortOrder, string searchCompany, string searchProduct)
        {
            ViewBag.CompanySortParm = String.IsNullOrEmpty(sortOrder) ? "company_desc" : "";
            ViewBag.ProductIdParm = sortOrder == "Product" ? "product_desc" : "Product";
            var companies = db.Companies.Include("Products").ToList();
            if (!String.IsNullOrEmpty(searchCompany))
                companies = companies.Where(c => c.Name.ToLower().Contains(searchCompany.ToLower())).ToList();
            if (!String.IsNullOrEmpty(searchProduct))
                companies.ForEach(c => c.Products = c.Products.Where(p => p.Name.ToLower().Contains(searchProduct.ToLower())).ToList());

            switch (sortOrder)
            {
                case "company_desc":
                    companies = companies.OrderByDescending(c => c.Name).ToList();
                    break;
                case "Product":
                    companies = companies.OrderBy(c => c.Name).ToList();
                    companies.ForEach(c => c.Products = c.Products.OrderBy(p => p.Code).ToList());
                    break;
                case "product_desc":
                    companies = companies.OrderBy(c => c.Name).ToList();
                    companies.ForEach(c => c.Products = c.Products.OrderByDescending(p => p.Code).ToList());                    
                    break;
                default:
                    companies = companies.OrderBy(c => c.Name).ToList();
                    break;
            }
            //ExcelHelper excelHelper = new ExcelHelper();
            //excelHelper.ImportExcel();
            return View(companies);
        }

    }
}