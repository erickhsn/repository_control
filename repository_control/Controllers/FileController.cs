﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using repository_control.Helpers;

namespace repository_control.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload()
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                    file.SaveAs(path);
                    ExcelHelper excelHelper = new ExcelHelper();
                    var errors = excelHelper.ImportExcel(path);
                    System.IO.File.Delete(path);
                    if(!errors.Equals(string.Empty))
                        return FileError(errors);
                    
                    
                }
            }
                       
            return RedirectToAction("Index");
        }

        public ActionResult FileError(string message)
        {
            return View(message);
        }
    }
}