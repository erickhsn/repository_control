﻿using GemBox.Spreadsheet;
using repository_control.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;

namespace repository_control.Helpers
{
    public class ExcelHelper
    {
        private AplicationDbContext db = new AplicationDbContext();
        public string ImportExcel(string path)
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            var workbook = ExcelFile.Load(path);

            var sb = new StringBuilder();

            string errors = string.Empty;

            List<Company> companies = new List<Company>();
            foreach (var worksheet in workbook.Worksheets)
            {
                foreach (var row in worksheet.Rows)
                {
                    if(row.Index >= 1)
                    {
                        var empresa = row.AllocatedCells.ElementAt(0);
                        Company company = db.Companies.Where(x => x.Name == empresa.StringValue).SingleOrDefault();
                        if (company == null)
                        {
                            company = new Company() { Name = empresa.StringValue };
                            db.Companies.Add(company);
                            db.SaveChanges();
                            company = db.Companies.Where(x => x.Name == empresa.StringValue).SingleOrDefault();
                        }

                        var produto_code = row.AllocatedCells.ElementAt(1);
                        Product product = db.Products.Where(x => x.Code == produto_code.IntValue && x.CompanyId == company.Id).SingleOrDefault();
                        if (product == null)
                        {
                            var produto_name = row.AllocatedCells.ElementAt(2).StringValue;
                            var produto_saida = row.AllocatedCells.ElementAt(3)?.Value != null ? row.AllocatedCells.ElementAt(3).IntValue : 0;
                            var produto_entrada = row.AllocatedCells.ElementAt(4)?.Value != null ? row.AllocatedCells.ElementAt(4).IntValue : 0;
                            var product_amount = row.AllocatedCells.ElementAt(5)?.IntValue;
                            if (product_amount != null)
                            {
                                product = new Product() { Name = produto_name, Code = produto_code.IntValue, Amount = int.Parse(product_amount + ""), CompanyId = company.Id};
                                db.Products.Add(product);
                                db.SaveChanges();
                            }
                            else
                            {
                                return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                            }
                        }
                        else
                        {
                            var produto_name = row.AllocatedCells.ElementAt(2).StringValue;
                            var produto_entrada = row.AllocatedCells.ElementAt(3)?.Value != null ? row.AllocatedCells.ElementAt(3).IntValue : 0;
                            var produto_saida = row.AllocatedCells.ElementAt(4)?.Value != null ? row.AllocatedCells.ElementAt(4).IntValue : 0;
                            var product_amount = row.AllocatedCells.ElementAt(5)?.IntValue;
                            if (product_amount != null)
                            {
                                if (produto_saida != 0 && produto_entrada != 0 && product_amount != product.Amount)
                                {
                                    return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                                }
                                else if (produto_saida != 0)
                                {
                                    if ((product.Amount - produto_saida) == product_amount)
                                    {
                                        product.Amount = product_amount.Value;
                                        db.Entry(product).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                                    }
                                }
                                else if (produto_entrada != 0)
                                {
                                    if (produto_entrada + product.Amount == product_amount)
                                    {
                                        product.Amount = product_amount.Value;
                                        db.Entry(product).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                                    }
                                }
                                else
                                {
                                    return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                                }
                            }
                            else
                            {
                                return errors = string.Format("Erro ao atualizar produto estoque. Linha {0}", row.Index + 1);
                            }
                        }
                    }

                }
            }

            return errors;
        }
    }
}