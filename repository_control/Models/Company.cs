﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace repository_control.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}