﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace repository_control.Models
{
    public class Product
    {

        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }

        [ScriptIgnore]
        public Company Company { get; set; }
        public int CompanyId { get; set; }

    }
}