﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace repository_control.Models
{
    public class AplicationDbContext : DbContext
    {
        public AplicationDbContext() : base("DefaultConnection")
        {

        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .HasMany(c => c.Products)
                .WithRequired(e => e.Company);
        }
    }
}