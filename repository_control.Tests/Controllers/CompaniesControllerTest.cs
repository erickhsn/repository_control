﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using repository_control.Controllers;
using repository_control.Models;
using repository_control.Tests.RepositoryMock;

namespace repository_control.Tests.Controllers
{
    [TestClass]
    public class CompaniesControllerTest
    {
        [TestMethod]
        public void Create()
        {
            CompaniesController controller = new CompaniesController(new TestRepositoryAppContext());

            Company company = new Company() { Id = 1,Name = "Empresa Teste 1" };

            // Act
            var result = (RedirectToRouteResult)controller.Create(company);
            Assert.AreEqual("Index", result.RouteValues["action"].ToString());
        }

        [TestMethod]
        public void Index()
        {
            var context = new TestRepositoryAppContext();
            context.Companies.Add(new Company { Id = 1, Name = "Empresa 1"});
            context.Companies.Add(new Company { Id = 2, Name = "Empresa 2" });
            context.Companies.Add(new Company { Id = 3, Name = "Empresa 3" });

            var controller = new CompaniesController(context);
            var result = controller.Index() as ViewResult;
            var companies = (List<Company>)result.Model;

            Assert.AreEqual(3, companies.Count);
        }
    }
}
