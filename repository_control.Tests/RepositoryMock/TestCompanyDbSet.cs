﻿using repository_control.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace repository_control.Tests.RepositoryMock
{
    public class TestCompanyDbSet : TestDbSet<Company>
    {
        public override Company Find(params object[] keyValues)
        {
            return this.SingleOrDefault(company => company.Id == (int)keyValues.Single());
        }

        public override Company Add(Company item) => base.Add(item);
    }
}
