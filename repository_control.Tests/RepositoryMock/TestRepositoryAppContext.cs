﻿using repository_control.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace repository_control.Tests.RepositoryMock
{
    class TestRepositoryAppContext : AplicationDbContext
    {
        public TestRepositoryAppContext()
        {
            this.Companies = new TestCompanyDbSet();
        }


        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Company item) { }
        public void Dispose() { }

    }
}
